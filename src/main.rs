/*
Copyright 2020 by ITwrx (https://itwrx.org). Released under the terms of the GPLV3 only.

This file is part of Passwords-GTK.

Passwords-GTK is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as published by
the Free Software Foundation.

Passwords-GTK is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Passwords-GTK.  If not, see <https://www.gnu.org/licenses/>.
*/

extern crate passwords;
use gtk::prelude::*;
use gtk::{Window, WindowType, Box, Button, ButtonBox, Separator, SpinButton, Switch, Align, Image, Label};

use passwords::PasswordGenerator;
use passwords::analyzer;
use passwords::scorer;

fn main() {
    
    if gtk::init().is_err() {
        println!("Failed to initialize GTK.");
        return;
    }
    
    let gtk_window = Window::new(WindowType::Toplevel);    
    
    gtk_window.set_title("Passwords-GTK by ITwrx");    
    
    gtk_window.set_default_size(800, 400);    
    
    //load a stylesheet... 
    let css = include_bytes!("passwords_gtk.css");
    let screen = gtk_window.get_screen().unwrap();
    let style = gtk::CssProvider::new();
    style.load_from_data(css).unwrap();
    gtk::StyleContext::add_provider_for_screen(&screen, &style, gtk::STYLE_PROVIDER_PRIORITY_USER);    
    
    let wrap_box = Box::new(gtk::Orientation::Vertical, 10);
    let settings_box = Box::new(gtk::Orientation::Vertical, 10);
    
    let separator1 = Separator::new(gtk::Orientation::Horizontal);
    let separator2 = Separator::new(gtk::Orientation::Horizontal);
    let separator3 = Separator::new(gtk::Orientation::Horizontal);    
    
    let settings_btnbox1 = ButtonBox::new(gtk::Orientation::Horizontal);
    
    settings_btnbox1.set_valign(Align::Start);
    settings_btnbox1.set_layout(gtk::ButtonBoxStyle::Start);       
    
    let btn2_label = Label::new(Some("Length: "));
    btn2_label.set_tooltip_text(Some("How many characters should your generated password be?"));    
    
    let btn2_label_context = btn2_label.get_style_context();
    btn2_label_context.add_class("help_text");  
    btn2_label_context.add_class("first_widget"); 
    
    let btn2 = SpinButton::new_with_range(2.0, 200.0, 1.0);
    
    //set an underlying gtk::widget property for our btn
    btn2.set_halign(Align::Fill);        
    btn2.set_margin_start(4);
    btn2.set_value(31.0);    
    
    let btn2_context = btn2.get_style_context();    
    btn2_context.add_class("first_widget"); 
    
    settings_btnbox1.pack_start(&btn2_label, true, true, 1);
    settings_btnbox1.pack_start(&btn2, false, false, 1);
    
    let settings_btnbox2 = ButtonBox::new(gtk::Orientation::Horizontal);
    
    settings_btnbox2.set_valign(Align::Start);
    settings_btnbox2.set_layout(gtk::ButtonBoxStyle::Start);
    
    let btn3_label = Label::new(Some("Numbers? "));
    btn3_label.set_tooltip_text(Some("Should the random generator allow numbers to be included in the generated password?"));  
    let btn3_label_context = btn3_label.get_style_context();
    btn3_label_context.add_class("help_text");
    
    let btn3 = Switch::new();
    
    btn3.set_halign(Align::Fill);
    btn3.set_margin_start(4);
    btn3.set_active(true);
    
    let btn4_label = Label::new(Some("Lowercase? "));
    btn4_label.set_tooltip_text(Some("Should the random generator allow lowercase letters to be included in the generated password?"));  
    let btn4_label_context = btn4_label.get_style_context();
    btn4_label_context.add_class("help_text");
    
    let btn4 = Switch::new();
    
    btn4.set_halign(Align::Fill);
    btn4.set_margin_start(4);
    btn4.set_active(true);
    
    let btn5_label = Label::new(Some("Uppercase? "));
    btn5_label.set_tooltip_text(Some("Should the random generator allow uppercase letters to be included in the generated password?"));  
    let btn5_label_context = btn5_label.get_style_context();
    btn5_label_context.add_class("help_text");
    
    let btn5 = Switch::new();
    
    btn5.set_halign(Align::Fill);
    btn5.set_margin_start(4);
    btn5.set_active(true);   
    
    settings_btnbox2.pack_start(&btn3_label, false, false, 1);
    settings_btnbox2.pack_start(&btn3, false, false, 1);
    
    settings_btnbox2.pack_start(&btn4_label, false, false, 1);
    settings_btnbox2.pack_start(&btn4, false, false, 1);
    
    settings_btnbox2.pack_start(&btn5_label, false, false, 1);
    settings_btnbox2.pack_start(&btn5, false, false, 1);      
    
    let settings_btnbox3 = ButtonBox::new(gtk::Orientation::Horizontal);
    
    settings_btnbox3.set_valign(Align::Start);
    settings_btnbox3.set_layout(gtk::ButtonBoxStyle::Start);
    
    let btn6_label = Label::new(Some("Symbols? "));
    btn6_label.set_tooltip_text(Some("Should the random generator allow symbols/special characters to be included in the generated password?")); 
    let btn6_label_context = btn6_label.get_style_context();
    btn6_label_context.add_class("help_text");
    
    let btn6 = Switch::new();
    
    btn6.set_halign(Align::Fill);
    btn6.set_margin_start(4);
    btn6.set_active(true);
    
    let btn7_label = Label::new(Some("Spaces? "));
    btn7_label.set_tooltip_text(Some("Should the random generator allow spaces to be included in the generated password?"));
    let btn7_label_context = btn7_label.get_style_context();
    btn7_label_context.add_class("help_text");   
    
    let btn7 = Switch::new();
    
    btn7.set_halign(Align::Fill);
    btn7.set_margin_start(4);
    btn7.set_active(false);
    
    let btn8_label = Label::new(Some("Exclude Similar? "));
    btn8_label.set_tooltip_text(Some("Should the random generator exclude characters that look similar to each other? This makes it harder to mix up zeros and Os, for instance, but reduces password complexity. Should probably only ever be considered for passwords which will usually be hand typed.")); 
    let btn8_label_context = btn8_label.get_style_context();
    btn8_label_context.add_class("help_text");
    
    let btn8 = Switch::new();
    
    btn8.set_halign(Align::Fill);
    btn8.set_margin_start(4);
    btn8.set_active(false);
    
    let btn9_label = Label::new(Some("Strict? "));
    btn9_label.set_tooltip_text(Some("Should the random generator make allowed characters mandatory, instead of only possibly included in the generated password?"));
    let btn9_label_context = btn9_label.get_style_context();
    btn9_label_context.add_class("help_text");    
    
    let btn9 = Switch::new();
    
    btn9.set_halign(Align::Fill);
    btn9.set_margin_start(4);
    btn9.set_active(true);
    
    settings_btnbox3.pack_start(&btn6_label, false, false, 1);
    settings_btnbox3.pack_start(&btn6, false, false, 1);
    
    settings_btnbox3.pack_start(&btn7_label, false, false, 1);
    settings_btnbox3.pack_start(&btn7, false, false, 1);
    
    settings_btnbox3.pack_start(&btn8_label, false, false, 1);
    settings_btnbox3.pack_start(&btn8, false, false, 1);
    
    settings_btnbox3.pack_start(&btn9_label, false, false, 1);
    settings_btnbox3.pack_start(&btn9, false, false, 1);
    
    let gen_box = Box::new(gtk::Orientation::Horizontal, 0);
    gen_box.set_halign(Align::Center);
    
    let pass_box = Box::new(gtk::Orientation::Horizontal, 0);  
    pass_box.set_halign(Align::Center);
    
    let spacer_box1 = Box::new(gtk::Orientation::Horizontal, 0);
    let spacer_box2 = Box::new(gtk::Orientation::Horizontal, 0);
    let spacer_box3 = Box::new(gtk::Orientation::Horizontal, 0);
    
    //let gen_btn_img = Image::new_from_icon_name(Some("system-run"), gtk::IconSize::LargeToolbar);
    let gen_btn_img = Image::new_from_icon_name(Some("system-run"), gtk::IconSize::Dnd);
    
    let gen_btn = Button::new_with_label("Generate");
    gen_btn.set_halign(Align::Center);
    
    gen_btn.set_always_show_image(true);
    
    //attach the image to the btn
    gen_btn.set_image(Some(&gen_btn_img));   
    
    let gen_btn_context = gen_btn.get_style_context();
    gen_btn_context.add_class("gen_btn");    
    
    let copy_btn_img = Image::new_from_icon_name(Some("edit-copy"), gtk::IconSize::Button); 
        
    let copy_btn = Button::new();     
    copy_btn.set_always_show_image(true);
    copy_btn.set_margin_start(8);    
    
    copy_btn.set_image(Some(&copy_btn_img));  
    
    let copy_btn_clone = copy_btn.clone();
    
    let pass_label = Label::new(Some("Password: "));
    pass_label.set_margin_start(8);
    
    let pass_label_context = pass_label.get_style_context();
    pass_label_context.add_class("pass_label");
    
    //let passwd = Label::new(None);
    let passwd = Label::new(Some("Choose your settings and click 'Generate' above."));   
    
    let passwd_context = passwd.get_style_context();
    passwd_context.add_class("passwd");   

    //the settings box holds the settings_btnboxes.
    settings_box.pack_start(&settings_btnbox1, false, false, 1);
    settings_box.pack_start(&separator1, true, true, 1);
    settings_box.pack_start(&settings_btnbox2, false, false, 1);
    settings_box.pack_start(&separator2, true, true, 1);
    settings_box.pack_start(&settings_btnbox3, false, false, 1); 
    settings_box.pack_start(&separator3, true, true, 1);
    
    //the gen box holds the gen btn.    
    gen_box.pack_start(&gen_btn, false, false, 1);    
    
    //the pass_box holds the passwd label, the passwd and the copy_btnbox.    
    pass_box.pack_start(&pass_label, false, false, 1);
    pass_box.pack_start(&passwd, false, false, 1);   
    pass_box.pack_start(&copy_btn, false, false, 1);       
    
    //let score_box = Box::new(gtk::Orientation::Vertical, 0);
    let score_box1 = Box::new(gtk::Orientation::Horizontal, 0);
    let score_box2 = Box::new(gtk::Orientation::Horizontal, 0);
    
    let score_box2_clone = score_box2.clone();
    
    let score_label = Label::new(None);    
    let score_level_a = Label::new(None);
    let score_level_b = Label::new(None);
    score_level_a.set_tooltip_text(Some("Classification Scale: Very Dangerous -> Dangerous -> Very Weak -> Weak -> Good -> Strong -> Very Strong -> Invulnerable"));
    let score_level_a_context = score_level_a.get_style_context();
    score_level_a_context.add_class("help_text");   
    
    score_box1.pack_start(&score_label, false, false, 1);    
    score_box2.pack_start(&score_level_a, false, false, 1);
    score_box2.pack_start(&score_level_b, false, false, 1);
    
    score_box1.set_halign(Align::Center);
    score_box2.set_halign(Align::Center);
    
    //the wrap_box holds the settings_box and the gen_box.
    wrap_box.pack_start(&settings_box, false, false, 1);
    wrap_box.pack_start(&gen_box, false, false, 1); 
    wrap_box.pack_start(&spacer_box1, false, false, 1);
    wrap_box.pack_start(&pass_box, false, false, 1);
    wrap_box.pack_start(&spacer_box2, false, false, 1);   
    wrap_box.pack_start(&score_box1, false, false, 1);
    wrap_box.pack_start(&score_box2, false, false, 1);
    wrap_box.pack_start(&spacer_box3, false, false, 1);
    
    //add the wrap_box to the gtk window.
    gtk_window.add(&wrap_box);       
    
    gen_btn.connect_clicked(move |_| {           
        
        let btn2_float = btn2.get_value();
        let btn2_int = btn2_float as usize;
        
        let pg = PasswordGenerator {            
            
           length: btn2_int,
           numbers: btn3.get_state(),
           lowercase_letters: btn4.get_state(),
           uppercase_letters: btn5.get_state(),
           symbols: btn6.get_state(),
           spaces: btn7.get_state(),
           exclude_similar_characters: btn8.get_state(),
           strict: btn9.get_state(),
       };
        
        let pass = pg.generate_one().unwrap(); 

        let pass_clone = pass.clone();        
        
        passwd.set_selectable(true);        
        passwd.set_text(&pass);          
        
        let passwd_clone = passwd.clone();
        
        copy_btn_clone.show(); 
        
        copy_btn_clone.connect_clicked(move |_| {
            
            let atom = gdk::Atom::intern("CLIPBOARD");
            let clipboard = gtk::Clipboard::get(&atom);
            
            clipboard.set_text(&pass_clone);
            passwd_clone.select_region(0,-1);
        });
            
        let score = scorer::score(&analyzer::analyze(&pass));
            
        let score_int = score as usize;            
            
        let score_string = format!("Your password's score is {}", &score_int);
            
        score_label.set_text(&score_string); 
        
        score_level_a.set_text("Classification: ");
            
        match &score_int {        
            // Match an inclusive range
            0..=20 => score_level_b.set_text("Very Dangerous."),
            20..=40 => score_level_b.set_text("Dangerous."),
            40..=60 => score_level_b.set_text("Very Weak."),
            60..=80 => score_level_b.set_text("Weak."),
            80..=90 => score_level_b.set_text("Good."),
            90..=95 => score_level_b.set_text("Strong."),
            95..=99 => score_level_b.set_text("Very Strong."),
            99..=100 => score_level_b.set_text("Invulnerable."),
            // Handle the rest of cases
            _ => println!("?"),
        }         
        
        score_box2_clone.show();
        
    });
    
    gtk_window.show_all();
    copy_btn.hide();
    score_box2.hide();
    
    //destroy the whole window/gui.
    gtk_window.connect_delete_event(|_, _| {
        gtk::main_quit();
        Inhibit(false)
    });
    
    gtk::main();
    
}
