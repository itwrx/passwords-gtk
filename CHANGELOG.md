## [Version 0.9.1] - 4-20-2020
### Added
-styled labels that have help text (yellow with dark background). Works on both light and dark GTK desktop themes.
-added copy-to-clipboard button for generated password. 
-password highlighted upon copy to indicate that to the user.
-improved layout/spacing.
-signed the binary and including sha256sum.
-added CHANGELOG.md

## [Version 0.9.0] - Initial Release - aprox. 3-20-2020?
### Added
-generate password.
-tooltip help text on some labels.
-score password.
