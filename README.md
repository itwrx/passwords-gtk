# Passwords-GTK

![Passwords-GTK in Gnome DE](Passwords-GTK-lrg-screenshot.png "Passwords-GTK in Gnome DE")

Description
-----------------
Passwords-GTK is a GTK GUI (Graphical User Interface) for the Passwords Rust Crate.  
It is intended as a quick and easy way to create secure passwords.  
It generates and scores passwords based on user selections.
It currently doesn't save settings, passwords, or automatically enter passwords into websites. 
This is just a simple project used for learning Rust and GTK and may or may not gain new features. 


Compatibility
-----------------
The current binary was built for 64 bit Gnu+Linux. Other OSes are not officially supported, and users of those would have to build their own binary from source.


Usage
------------------
You can download and run the pre-built binary, or you can build the binary yourself from the sources in this repo. You can optionally integrate the software into your Gnome desktop environment, if you are using that.

Please see the "instructions" directory for details.


Feedback
------------------
Gnu+Linux users should feel free to report issues and submit feature requests.
Constructive criticism is appreciated.


------------------
Thank you for your interest in this little program and we hope some people find it useful.
